rm:
	docker-compose rm --all -f

build:
	docker-compose build
	cd jenkins-java8-builder && docker build --tag jenkins-java8-builder:build .

up:
	docker-compose up

all: rm build up
