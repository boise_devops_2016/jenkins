#!groovy

/*
   Automatically configure the docker cloud in Jenkins.

   Tested with:
   - {name: 'docker-plugin', ver: '0.16.0'}
 */

import jenkins.model.*;
import hudson.model.*;
import com.nirima.jenkins.plugins.docker.DockerCloud
import com.nirima.jenkins.plugins.docker.DockerTemplate
import com.nirima.jenkins.plugins.docker.DockerTemplateBase
import com.nirima.jenkins.plugins.docker.launcher.DockerComputerSSHLauncher
import hudson.plugins.sshslaves.SSHConnector
import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.common.*
import com.cloudbees.plugins.credentials.domains.*
import com.cloudbees.plugins.credentials.impl.*

def instance = Jenkins.getInstance()

if ( instance.pluginManager.activePlugins.find { it.shortName == "credentials" } != null ) {

	def domain = Domain.global()
	def store = instance.getExtensionList('com.cloudbees.plugins.credentials.SystemCredentialsProvider')[0].getStore()

	// https://github.com/jenkinsci/credentials-plugin/blob/3a9ce5254749da296a76f55e3dfdae3d496b695a/src/main/java/com/cloudbees/plugins/credentials/impl/UsernamePasswordCredentialsImpl.java#L68
	slaveUsernameAndPassword = new UsernamePasswordCredentialsImpl(
							CredentialsScope.GLOBAL,
							"jenkins-java8-builders",
							"Jenkins Slave with Password Configuration",
							"jenkins",
							"jenkins"
					)
	println '--> adding credentials for ssh slaves'
	store.addCredentials(domain, slaveUsernameAndPassword)

} else {
	println "--> no credentials plugin installed"
}

if ( instance.pluginManager.activePlugins.find { it.shortName == "docker-plugin" } != null ) {

	// https://github.com/jenkinsci/docker-plugin/blob/docker-plugin-parent-0.16.0/docker-plugin/src/main/java/com/nirima/jenkins/plugins/docker/DockerTemplateBase.java#L103
	DockerTemplateBase templateBase = new DockerTemplateBase(
								"jenkins-java8-builder",
								"",
								"",
								"""/var/run/docker.sock:/var/run/docker.sock""",
								"",
								"",
								"",
								"",
                "docker",
								2048,
								0,
								1024,
								"",
								false,
								false,
								false,
								''
					);

	// https://github.com/jenkinsci/ssh-slaves-plugin/blob/ssh-slaves-1.11/src/main/java/hudson/plugins/sshslaves/SSHConnector.java#L178
	SSHConnector sshConnector = new SSHConnector(22,
						"docker-slave",
						"",
						"",
						"",
						"",
						0,
						0,
						0
					);

	// https://github.com/jenkinsci/docker-plugin/blob/docker-plugin-parent-0.16.0/docker-plugin/src/main/java/com/nirima/jenkins/plugins/docker/launcher/DockerComputerSSHLauncher.java#L43
	DockerComputerSSHLauncher dkSSHLauncher = new DockerComputerSSHLauncher(sshConnector);

	// https://github.com/jenkinsci/docker-plugin/blob/docker-plugin-parent-0.16.0/docker-plugin/src/main/java/com/nirima/jenkins/plugins/docker/DockerTemplate.java#L71
	DockerTemplate dkTemplate = new DockerTemplate(
						templateBase,
						"docker",
						"",
						"",
						"99"
					)
	dkTemplate.setLauncher(dkSSHLauncher);

	ArrayList<DockerTemplate> dkTemplates = new ArrayList<DockerTemplate>();
	dkTemplates.add(dkTemplate);

	ArrayList<DockerCloud> dkCloud = new ArrayList<DockerCloud>();
	dkCloud.add(
		// https://github.com/jenkinsci/docker-plugin/blob/docker-plugin-parent-0.16.0/docker-plugin/src/main/java/com/nirima/jenkins/plugins/docker/DockerCloud.java#L122
		new DockerCloud(
				"dk",
				dkTemplates,
				"unix:///var/run/docker.sock",
				"",
				5,
				5,
				"",
				""
			)
	);

	println '--> Configuring docker cloud'
	// Took me aaaaages to work out how to add/replace a 'cloud' in Jenkins settings
	// https://github.com/jenkinsci/jenkins/blob/9fce1ee933eb5276baff977d562fc8e183f1c8d6/core/src/main/java/jenkins/model/Jenkins.java#L485
	// https://github.com/jenkinsci/jenkins/blob/9fce1ee933eb5276baff977d562fc8e183f1c8d6/core/src/main/java/hudson/model/Hudson.java#L336
	// https://github.com/jenkinsci/jenkins/blob/9fce1ee933eb5276baff977d562fc8e183f1c8d6/core/src/main/java/jenkins/model/Jenkins.java#L487
	// https://github.com/jenkinsci/jenkins/blob/9fce1ee933eb5276baff977d562fc8e183f1c8d6/core/src/main/java/hudson/util/DescribableList.java#L66
	// https://github.com/jenkinsci/jenkins/blob/9fce1ee933eb5276baff977d562fc8e183f1c8d6/core/src/main/java/hudson/util/PersistedList.java#L83
	instance.clouds.replaceBy(dkCloud)

} else {
	println "--> no 'docker-plugin' plugin installed"
}
